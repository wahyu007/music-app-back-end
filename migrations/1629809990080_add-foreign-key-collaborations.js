/* eslint-disable camelcase */

exports.shorthands = undefined;

exports.up = (pgm) => {
  // memberikan constraint foreign key pada playlist_id terhapdap kolom id tabel playlists
  pgm.addConstraint('collaborations', 'fk_collaboration.playlists.id', 'FOREIGN KEY(playlist_id) REFERENCES playlists(id) ON DELETE CASCADE');

  // memberikan constraint foreign key pada song_id terhapdap kolom id tabel songs
  pgm.addConstraint('collaborations', 'fk_collaboration.users.id', 'FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE');
};

exports.down = (pgm) => {
  // menghapus constraint foreign key pada playlist_id terhapdap kolom id tabel playlists
  pgm.dropConstraint('collaborations', 'fk_collaboration.playlists.id');

  // menghapus constraint foreign key pada song_id terhapdap kolom id tabel songs
  pgm.dropConstraint('collaborations', 'fk_collaboration.users.id');
};
