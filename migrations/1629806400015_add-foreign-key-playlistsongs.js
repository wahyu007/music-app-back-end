/* eslint-disable camelcase */

exports.shorthands = undefined;

exports.up = (pgm) => {
  // memberikan constraint foreign key pada playlist_id terhapdap kolom id tabel playlists
  pgm.addConstraint('playlistsongs', 'fk_playlistsong.playlist_member.id', 'FOREIGN KEY(playlist_id) REFERENCES playlists(id) ON DELETE CASCADE');

  // memberikan constraint foreign key pada song_id terhapdap kolom id tabel songs
  pgm.addConstraint('playlistsongs', 'fk_playlistsong.detail_song.id', 'FOREIGN KEY(song_id) REFERENCES songs(id) ON DELETE CASCADE');
};

exports.down = (pgm) => {
  // menghapus constraint foreign key pada playlist_id terhapdap kolom id tabel playlists
  pgm.dropConstraint('playlistsongs', 'fk_playlistsong.playlist_member.id');

  // menghapus constraint foreign key pada song_id terhapdap kolom id tabel songs
  pgm.dropConstraint('playlistsongs', 'fk_playlistsong.detail_song.id');
};
