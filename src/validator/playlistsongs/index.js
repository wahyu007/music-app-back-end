const InvarianError = require('../../exceptions/InvarianError');
const { PlaylistSongPayloadSchema } = require('./schema');

const PlaylistSongsValidator = {
  validatePlaylistSongPayload: (payload) => {
    const validationResult = PlaylistSongPayloadSchema.validate(payload);
    if (validationResult.error) {
      throw new InvarianError(validationResult.message);
    }
  },
};

module.exports = PlaylistSongsValidator;
