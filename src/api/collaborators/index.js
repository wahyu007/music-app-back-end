const CollaboratorsHandler = require('./handler');
const routes = require('./routes');

module.exports = {
  name: 'collaborators',
  version: '1.0.0',
  register: async (server, { service, servicePlaylist, validator }) => {
    const collaboratorsHandler = new CollaboratorsHandler(service, servicePlaylist, validator);
    server.route(routes(collaboratorsHandler));
  },
};
