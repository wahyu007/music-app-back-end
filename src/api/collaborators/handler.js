const ClientError = require('../../exceptions/ClientError');

class CollaboratorsHandler {
  constructor(service, servicePlaylist, validator) {
    this._service = service;
    this._servicePlaylist = servicePlaylist;
    this._validator = validator;

    this.postCollaborationHandler = this.postCollaborationHandler.bind(this);
    this.deleteCollaborationHandler = this.deleteCollaborationHandler.bind(this);
  }

  async postCollaborationHandler(request, h) {
    try {
      this._validator.validateCollaboratorPayload(request.payload);
      const {
        playlistId,
        userId,
      } = request.payload;

      const { id: credentialId } = request.auth.credentials;
      await this._servicePlaylist.verifyPlaylistOwner(playlistId, credentialId);

      const CollaboratorId = await this._service.addCollaborator({
        playlistId, userId,
      });

      const response = h.response({
        status: 'success',
        message: 'Collaborator berhasil ditambahkan',
        data: {
          CollaboratorId,
        },
      });

      response.code(201);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        });
        response.code(error.statusCode);
        return response;
      }

      const response = h.response({
        status: 'error',
        message: 'Maaf, terjadi kegagalan pada server kami.',
      });
      response.code(500);
      console.error(error);
      return response;
    }
  }

  async deleteCollaborationHandler(request, h) {
    try {
      this._validator.validateCollaboratorPayload(request.payload);
      const {
        playlistId,
        userId,
      } = request.payload;
      // const { id: credentialId } = request.auth.credentials;

      await this._service.deleteCollaboration({ playlistId, userId });

      return {
        status: 'success',
        message: 'Collaborator berhasil dihapus',
      };
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        });
        response.code(error.statusCode);
        return response;
      }
      // Server ERROR!
      const response = h.response({
        status: 'error',
        message: 'Maaf, terjadi kegagalan pada server kami.',
      });
      response.code(500);
      console.error(error);
      return response;
    }
  }
}

module.exports = CollaboratorsHandler;
