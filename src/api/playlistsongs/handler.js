const ClientError = require('../../exceptions/ClientError');

class PlaylistsHandler {
  constructor(servicePalylistSong, servicePlaylists, validator) {
    this._service = servicePalylistSong;
    this._servicePlaylist = servicePlaylists;
    this._validator = validator;

    this.postSongToPlaylistByIdHandler = this.postSongToPlaylistByIdHandler.bind(this);
    this.getSongsFromPlaylistHandler = this.getSongsFromPlaylistHandler.bind(this);
    this.deleteSongToPlaylistByIdHandler = this.deleteSongToPlaylistByIdHandler.bind(this);
  }

  async getSongsFromPlaylistHandler(request, h) {
    try {
      const { id } = request.params;
      const { id: credentialId } = request.auth.credentials;

      await this._servicePlaylist.verifyPlaylistOwnerAndCollaboration(id, credentialId);

      const songs = await this._service.getSongsFromPlaylist(id, credentialId);

      return {
        status: 'success',
        data: {
          songs,
        },
      };
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        });
        response.code(error.statusCode);
        return response;
      }

      const response = h.response({
        status: 'error',
        message: 'Maaf, terjadi kegagalan pada server kami.',
      });
      response.code(500);
      console.error(error);
      return response;
    }
  }

  async postSongToPlaylistByIdHandler(request, h) {
    try {
      this._validator.validatePlaylistSongPayload(request.payload);
      const {
        songId,
      } = request.payload;
      const { id: credentialId } = request.auth.credentials;
      const { id } = request.params;

      await this._servicePlaylist.verifyPlaylistOwnerAndCollaboration(id, credentialId);
      await this._service.verifyDuplikatSong(id, songId);

      const playlistSongId = await this._service.addSongToPlaylist({
        playlistId: id, songId, owner: credentialId,
      });

      const response = h.response({
        status: 'success',
        message: 'Playlist berhasil ditambahkan',
        data: {
          playlistSongId,
        },
      });

      response.code(201);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        });
        response.code(error.statusCode);
        return response;
      }

      const response = h.response({
        status: 'error',
        message: 'Maaf, terjadi kegagalan pada server kami.',
      });
      response.code(500);
      console.error(error);
      return response;
    }
  }

  async deleteSongToPlaylistByIdHandler(request, h) {
    try {
      this._validator.validatePlaylistSongPayload(request.payload);
      const {
        songId,
      } = request.payload;
      const { id: credentialId } = request.auth.credentials;
      const { id } = request.params;

      await this._servicePlaylist.verifyPlaylistOwnerAndCollaboration(id, credentialId);

      await this._service.deleteSongFromPlaylist({
        id, songId, owner: credentialId,
      });

      const response = h.response({
        status: 'success',
        message: 'Lagu berhasil dihapus dari playlist',
      });

      response.code(200);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        });
        response.code(error.statusCode);
        return response;
      }

      const response = h.response({
        status: 'error',
        message: 'Maaf, terjadi kegagalan pada server kami.',
      });
      response.code(500);
      console.error(error);
      return response;
    }
  }
}

module.exports = PlaylistsHandler;
