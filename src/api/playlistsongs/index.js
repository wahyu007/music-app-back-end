const PlaylistsHandler = require('./handler');
const routes = require('./routes');

module.exports = {
  name: 'playlistsongs',
  version: '1.0.0',
  register: async (server, { servicePalylistSong, servicePlaylists, validator }) => {
    const playlistsHandler = new PlaylistsHandler(servicePalylistSong, servicePlaylists, validator);
    server.route(routes(playlistsHandler));
  },
};
