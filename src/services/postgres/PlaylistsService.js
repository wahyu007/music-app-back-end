const { nanoid } = require('nanoid');
const { Pool } = require('pg');
const InvariantError = require('../../exceptions/InvarianError');
const NotFoundError = require('../../exceptions/NotFoundError');
const AuthorizationError = require('../../exceptions/AuthorizationError');
const { mapDBToModel } = require('../../utils');

class PlaylistsService {
  constructor() {
    this._pool = new Pool();
  }

  async addPlaylist({
    name, owner,
  }) {
    const id = `playlist-${nanoid(16)}`;

    const query = {
      text: 'INSERT INTO playlists VALUES($1, $2, $3) RETURNING id',
      values: [id, name, owner],
    };

    const result = await this._pool.query(query);

    if (!result.rows[0].id) {
      throw new InvariantError('Lagu gagal ditambahkan');
    }

    return result.rows[0].id;
  }

  async deleteSongFromPlaylist({
    id, songId,
  }) {
    const query = {
      text: 'DELETE FROM playlistsongs WHERE playlist_id = $1 AND song_id = $2 RETURNING id',
      values: [id, songId],
    };

    const result = await this._pool.query(query);

    if (!result.rows[0]) {
      throw new InvariantError('Lagu gagal dihapus. id tidak ditemukan');
    }
  }

  async getPlaylists(owner) {
    const query = {
      text: `SELECT playlists.id as id, playlists.name as name, users.username as username
      FROM playlists
      JOIN users ON playlists.owner = users.id
      WHERE playlists.owner = $1 OR playlists.id in (SELECT playlist_id FROM collaborations WHERE user_id = $1)`,
      values: [owner],
    };
    const result = await this._pool.query(query);
    return result.rows.map(mapDBToModel);
  }

  async getPlaylistById(id) {
    const query = {
      text: 'SELECT * FROM playlists WHERE id = $1',
      values: [id],
    };
    const result = await this._pool.query(query);

    if (!result.rowCount) {
      throw new NotFoundError('Lagu tidak ditemukan');
    }

    return result.rows.map(mapDBToModel)[0];
  }

  async deletePlaylistById(id, owner) {
    const query = {
      text: 'DELETE FROM playlists WHERE id = $1 AND owner = $2 RETURNING id',
      values: [id, owner],
    };

    const result = await this._pool.query(query);

    if (!result.rows.length) {
      throw new NotFoundError('Lagu gagal dihapus. Id tidak ditemukan');
    }
  }

  async verifyPlaylistOwner(id, ownerId) {
    const query = {
      text: 'SELECT id, name, owner FROM playlists WHERE id = $1',
      values: [id],
    };

    const result = await this._pool.query(query);
    if (!result || result.rowCount === 0) {
      throw new NotFoundError('Catatan tidak ditemukan');
    }

    const playlist = result.rows[0];
    if (playlist.owner !== ownerId) {
      throw new AuthorizationError('Anda tidak berhak mengakses resource ini');
    }
    return result.rows[0];
  }

  async verifyPlaylistCollaboration(id, ownerId) {
    const query = {
      text: 'SELECT * FROM collaborations WHERE playlist_id = $1 AND user_id = $2',
      values: [id, ownerId],
    };

    const result = await this._pool.query(query);
    if (result) {
      return result.rows[0];
    }

    return false;
  }

  async getPlaylist(id, ownerId) {
    const query = {
      text: 'SELECT id, name, owner FROM playlists WHERE id = $1 AND owner = $2',
      values: [id, ownerId],
    };

    const result = await this._pool.query(query);
    if (result) {
      return result.rows[0];
    }

    return false;
  }

  async verifyPlaylistOwnerAndCollaboration(id, ownerId) {
    const collaboration = await this.verifyPlaylistCollaboration(id, ownerId);
    const playlist = await this.getPlaylist(id, ownerId);

    if ((collaboration === undefined) && (playlist === undefined)) {
      throw new AuthorizationError('Anda tidak berhak mengakses resource ini');
    }
  }
}

module.exports = PlaylistsService;
