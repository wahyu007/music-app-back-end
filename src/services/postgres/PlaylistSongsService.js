const { nanoid } = require('nanoid');
const { Pool } = require('pg');
const InvariantError = require('../../exceptions/InvarianError');
const NotFoundError = require('../../exceptions/NotFoundError');
const { mapDBToModel } = require('../../utils');
const ClientError = require('../../exceptions/ClientError');

class PlaylistSongsService {
  constructor(cacheService) {
    this._pool = new Pool();
    this._cacheService = cacheService;
  }

  async addSongToPlaylist({
    playlistId, songId, owner,
  }) {
    const id = `playlistSong-${nanoid(16)}`;

    const query = {
      text: 'INSERT INTO playlistsongs VALUES($1, $2, $3) RETURNING id',
      values: [id, playlistId, songId],
    };

    const result = await this._pool.query(query);

    if (!result.rows[0].id) {
      throw new InvariantError('Lagu gagal ditambahkan');
    }

    await this._cacheService.delete(`playlists:${playlistId}:${owner}`);
    return result.rows[0].id;
  }

  async deleteSongFromPlaylist({
    id, songId, owner,
  }) {
    const query = {
      text: 'DELETE FROM playlistsongs WHERE playlist_id = $1 AND song_id = $2 RETURNING id',
      values: [id, songId],
    };

    const result = await this._pool.query(query);

    if (!result.rows[0]) {
      throw new InvariantError('Lagu gagal dihapus. id tidak ditemukan');
    }
    await this._cacheService.delete(`playlists:${id}:${owner}`);
    // return result.rows[0];
  }

  async getSongsFromPlaylist(id, owner) {
    try {
      // mendapatkan catatan dari cache
      const result = await this._cacheService.get(`playlists:${id}:${owner}`);
      return JSON.parse(result);
    } catch (error) {
      const query = {
        text: `SELECT playlistsongs.song_id as id, songs.title as title, songs.performer 
        FROM playlistsongs JOIN songs ON playlistsongs.song_id = songs.id WHERE playlist_Id = $1`,
        values: [id],
      };
      const result = await this._pool.query(query);

      if (!result.rowCount) {
        throw new NotFoundError('Lagu tidak ditemukan');
      }

      const mappedResult = result.rows.map(mapDBToModel);

      await this._cacheService.set(`playlists:${id}:${owner}`, JSON.stringify(mappedResult));

      return mappedResult;
    }
  }

  async verifyDuplikatSong(id, songId) {
    const query = {
      text: 'SELECT id FROM playlistsongs WHERE playlist_id = $2 AND song_id = $1',
      values: [songId, id],
    };

    const result = await this._pool.query(query);

    if (result.rows.length) {
      throw new ClientError('Tidak dapat Menambahkan Lagu. lagu sudah ada diplaylist');
    }
  }
}

module.exports = PlaylistSongsService;
