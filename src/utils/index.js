/* eslint-disable camelcase */
const mapDBToModel = ({
  id,
  name,
  username,
  owner,
  title,
  year,
  performer,
  genre,
  duration,
  insertedAt,
  updatedAt,
}) => ({
  id,
  name,
  username,
  owner,
  title,
  year,
  performer,
  genre,
  duration,
  insertedAt,
  updatedAt,
});

module.exports = { mapDBToModel };
